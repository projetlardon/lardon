<?php
//verifier le token
session_start();
$tokenEnvoye = filter_input(INPUT_POST, "token");
if ($tokenEnvoye != $_SESSION["token"]) {
    echo "Vilain pirate";
    die();
}

// Reccup des données entrées dans le formulaire
$id = filter_input(INPUT_POST, "id");
$login = filter_input(INPUT_POST, "login");
$mdp = filter_input(INPUT_POST, "mdp");

echo $login;
die();

require "../../Config.php";

//Requete permettant de vérifier si l'ID et le mdp rentrés sont bons
$db = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);
$requete = $db -> prepare("select Admin, Login, MDP from personne where Login=:Login and MDP=:MDP");
$requete -> bindParam(":login", $login);
$requete -> bindParam(":mdp", $mdp);
$requete ->execute();

$lignes = $requete->fetchAll();

//Renvoie sur la bonne page
if($requete -> rowCount()==0){
    header('Location: Index.php?erreur_de_connexion');
}else {
    $admin = $lignes[0]["Admin"];
    $_SESSION['Login'] = $login;
    if ($admin == 1) {
        header('Location: Admin/Accueil/Index.php');
    } else {
        header('Location: Etudiant/Accueil/Index.php');
    }
}
?>