<?php
//pour puvoir me servir de $_SESSION
session_start();

include "HeaderCo.php";
mon_header("Nouveau compte");

//generation d'un token de securite
$token = rand(0, 200000000);
//je stocke la valeur en session, cote serveur
$_SESSION["token"] = $token;
?>

    <h1>Création d'un compte</h1>

    <form method="post" action="actions/actionNouveau.php">
        <input type="hidden" name="token" value="<?php echo $token ?>">
        <div class="form-group">
            <label for="nom">Nom</label>
            <input type="text" class="form-control" id="nom" maxlength="50"
                   name="nom"
                   placeholder="Nom..." required>
            <!--ne peut pas etre envoye si ce champ n'est pas rempli-->
        </div>
        <div class="form-group">
            <label for="prenom">Prénom</label>
            <input type="text" class="form-control" id="prenom" maxlength="50"
                   name="prenom"
                   placeholder="Prénom..." required>
        </div>
        <div class="form-group">
            <label for="login">Login</label>
            <input type="text" class="form-control" id="login" maxlength="50"
                   name="login"
                   placeholder="Login..." required>
        </div>
        <div class="form-group">
            <label for="mdp">Mot de passe</label>
            <input type="password" class="form-control" id="mdp" maxlength="20"
                   name="mdp"
                   placeholder="Mot de passe..." required>
        </div>
        <input type="checkbox" name="Admin[]" value="Admin">Admin <br>
        <!--    <div class="form-check">-->
        <!--        <input class="form-check-input" type="checkbox" id="checkAdmin">-->
        <!--        <label class="form-check-label" for="checkAdmin">-->
        <!--            Admin-->
        <!--        </label>-->
        <!--    </div>-->


        <a href="../Admin/Accueil/Index.php" class="btn btn-danger pull-left">
            <i class="fal fa-long-arrow-left"></i>
            Retour
        </a>
        <button type="submit" class="btn btn-primary pull-right">Créer</button>
    </form>

<?php
include '../Footer.php';
mon_footer();
?>