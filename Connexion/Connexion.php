<link rel="stylesheet" href="../Style.css">
<?php
//pour puvoir me servir de $_SESSION
session_start();

include "HeaderCo.php";
mon_header("Connexion");

//generation d'un token de securite
$token = rand(0, 200000000);
//je stocke la valeur en session, cote serveur
$_SESSION["token"] = $token;
?>

<form method="post" action="actions/actionVerifConnexion.php">
    <div class="form-group">
        <label for="login">Login</label>
        <input type="text" class="form-control" id="login" maxlength="50">
    </div>
    <div class="form-group">
        <label for="mdp">Mot de passe</label>
        <input type="password" class="form-control" id="mdp" maxlength="20">
    </div>
    <button type="submit" class="btn btn-primary">Se connecter</button>
</form>

<?php
include '../Footer.php';
mon_footer();
?>