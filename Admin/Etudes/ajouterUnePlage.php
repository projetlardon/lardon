<?php
//pour puvoir me servir de $_SESSION
session_start();

include "../HeaderAd.php";
mon_header("Ajouter une plage");

//generation d'un token de securite
$token = rand(0, 200000000);
//je stocke la valeur en session, cote serveur
$_SESSION["token"] = $token;
?>

<h1>Nouvelle plage</h1>

<form method="post" action="actions/actionAjoutPlage.php">
    <input type="hidden" name="token" value="<?php echo $token ?>">
    <div class="form-group">
        <label for="nom">Nom</label>
        <input type="text" class="form-control" id="nom" maxlength="50"
               name="nom"
               placeholder="Nom..." required>
        <!--ne peut pas etre envoye si ce champ n'est pas rempli-->
    </div>
    <div class="form-group">
        <label for="ville">Ville</label>
        <input type="text" class="form-control" id="ville" maxlength="50"
               name="ville"
               placeholder="Ville..." required>
    </div>
    <div class="form-group">
        <label for="departement">Département</label>
        <input type="text" class="form-control" id="departement" maxlength="50"
                  name="departement"
                  placeholder="Département...">
    </div>

    <a href="Index.php" class="btn btn-danger pull-left">
        <i class="fal fa-long-arrow-left"></i>
        Retour
    </a>
    <button type="submit" class="btn btn-primary pull-right">Ajouter</button>
</form>

<?php
include '../../Footer.php';
mon_footer();
?>
