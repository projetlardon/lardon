<link rel="stylesheet" href="../../Style.css">
<?php
include "../HeaderAd.php";
mon_header("Titre etude selec");
?>

<h1>Plages</h1>

<?php
require "../../Config.php";
//creer l'objet PDO qui me connecte a la BDD
$db = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);

$requete = $db->prepare("select id, Nom, Ville, Departement from plage");
$requete->execute();

$lignes = $requete->fetchAll();

foreach ($lignes

         as $ligne) {
    ?>


    <div id="btn">
        <a href="../Zone/Index.php">
            <button type="button" class="list-group-item list-group-item-action list-group-item-dark">
                <?php echo $ligne["Nom"] ?>
            </button>
        </a>
    </div>

    <?php
}
?>
</table>


<a href="ajouterUnePlage.php" class="btn btn-success">
    <i class="far fa-plus-circle"></i>
    Nouvelle plage
</a>

<?php
include '../../Footer.php';
mon_footer();
?>