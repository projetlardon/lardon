<?php
//pour puvoir me servir de $_SESSION
session_start();

include "../HeaderAd.php";
mon_header("Ajouter une étude");

//generation d'un token de securite
$token = rand(0, 200000000);
//je stocke la valeur en session, cote serveur
$_SESSION["token"] = $token;
?>

<h1>Nouvelle étude</h1>

<form method="post" action="actions/actionAjoutEtude.php">
    <input type="hidden" name="token" value="<?php echo $token ?>">
    <div class="form-group">
        <label for="titre">Titre</label>
        <input type="text" class="form-control" id="titre" maxlength="50"
               name="titre"
               placeholder="Titre..." required>
        <!--ne peut pas etre envoye si ce champ n'est pas rempli-->
    </div>
    <div class="form-group">
        <label for="dateDebut">Date de début</label>
        <input type="date" class="form-control" id="dateDebut" maxlength="8"
               name="dateDebut"
               placeholder="Date de début..." required>
    </div>
    <div class="form-group">
        <label for="dateFin">Date de fin</label>
        <input type="date" class="form-control" id="dateFin" maxlength="8"
                  name="dateFin"
                  placeholder="Date de fin...">
    </div>

    <a href="Index.php" class="btn btn-danger pull-left">
        <i class="fal fa-long-arrow-left"></i>
        Retour
    </a>
    <button type="submit" class="btn btn-primary pull-right">Ajouter</button>
</form>

<?php
include '../../Footer.php';
mon_footer();
?>
