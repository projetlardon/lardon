<?php

//verifier le token
session_start();
$tokenEnvoye = filter_input(INPUT_POST, "token");
if ($tokenEnvoye != $_SESSION["token"]) {
    echo "Vilain pirate";
    die();
}

//je vais recuperer les valeurs dans POST
$id = filter_input(INPUT_POST, "id");
$titre = filter_input(INPUT_POST, "titre");
$dateDebut = filter_input(INPUT_POST, "dateDebut");
$dateFin = filter_input(INPUT_POST, "dateFin");

//je vais inserer cette nouvelle categorie dans la BDD
require "../../../Config.php";

//creer l'objet PDO qui me connecte a la BDD
$db = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);

//preparer une requete
$requete = $db->prepare("insert into etude(Titre, dateDebut, dateFin)" . " values ('" . $titre . "','" . $dateDebut . "', '" . $dateFin . "')");
$requete->bindParam(":titre", $titre);
$requete->bindParam(":dateDebut", $dateDebut);
$requete->bindParam(":dateFin", $dateFin);


$requete->execute();

//puis je vais retournui a l'accueil (la liste des categories)
header("location: ../Index.php");