<link rel="stylesheet" href="../Style.css">
<?php
include "../HeaderEt.php";
mon_header("Titre etude selec");
?>

<h1>Zones</h1>

<?php
require "../../Config.php";
//creer l'objet PDO qui me connecte a la BDD
$db = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);

$requete = $db->prepare("select id, C1latitude, C2latitude, C3latitude, C4latitude, C1longitude,
 C2longitude, C3longitude, C4longitude, NombreEtudiant, id_personne, id_plage, id_etude from zone");
$requete->execute();

$lignes = $requete->fetchAll();

foreach ($lignes

         as $ligne) {
    ?>


    <div id="btn">
        <a href="../Zone/Index.php">
            <button type="button" class="list-group-item list-group-item-action active list-group-item-info">
                <?php echo $ligne["Nom"] ?>
            </button>
        </a>
    </div>

    <?php
}
?>
</table>


<a href="ajouterZone.php" class="btn btn-success">
    <i class="far fa-plus-circle"></i>
    Nouvelle zone
</a>
<a href="ajouterUneEspece.php" class="btn btn-success">
    <i class="far fa-plus-circle"></i>
    Nouvelle espèce
</a>

<?php
include '../../Footer.php';
mon_footer();
?>