<?php
//pour puvoir me servir de $_SESSION
session_start();

include "../HeaderEt.php";
mon_header("Ajouter une espèce");

//generation d'un token de securite
$token = rand(0, 200000000);
//je stocke la valeur en session, cote serveur
$_SESSION["token"] = $token;
?>

<h1>Nouvelle espece</h1>

<form method="post" action="actions/actionAjoutEspece.php">
    <input type="hidden" name="token" value="<?php echo $token ?>">
    <div class="form-group">
        <label for="nom">Nom</label>
        <input type="text" class="form-control" id="nom" maxlength="50"
               name="nom"
               placeholder="Nom..." required>
    </div>
    <input type="hidden" name="token" value="<?php echo $token ?>">
    <div class="form-group">
        <label for="number">Nombre</label>
        <input type="text" class="form-control" id="nombre" maxlength="255"
               name="nombre0"
               placeholder="Nombre..." required>
    </div>


    <a href="Index.php" class="btn btn-danger pull-left">
        <i class="fal fa-long-arrow-left"></i>
        Retour
    </a>
    <button type="submit" class="btn btn-primary pull-right">Ajouter</button>
</form>

<?php
include '../../Footer.php';
mon_footer();
?>
