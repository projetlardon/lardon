<?php
session_start();
include "../HeaderEt.php";
mon_header("Accueil de la photothèque de chatons");


//Génération d'un token de sécurité
$token=rand(0,2000000000);
//je stocke la valeur en session, côté serveur
$_SESSION["token"]=$token;
?>

<h1>Liste des zones</h1>

<table class="table">
    <tr>
        <th></th>
        <th></th>
    </tr>
    <?php
    require "config.php";
    //créer l'objet PDO qui me connecte à la BDD
    $db = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);

    $requete = $db->prepare("select id, C1latitude, C2latitude, C3latitude, C4latitude, C1longitude, C2longitude, C3longitude, C4longitude, NombreEtudiant, id_personne, id_plage, id_etude from zone");
    $requete->execute();

    $lignes = $requete->fetchAll();

    //var_dump($lignes);
    foreach ($lignes as $ligne){
        ?>
        <tr>
            <td><?php echo $ligne["id"] ?></td>
            <td>
                <a href="modifierUneCategorie.php?id=<?php echo $ligne["id"] ?>"
                   class="btn btn-primary"><i class="fa fa-pen"></i></a>

                <a
                        href="supprimerUneCategorie.php?id=<?php echo $ligne["id"] ?>"
                        class="btn btn-danger"><i class="fa fa-trash"></i> Version page de confirmation</a>

                <form method="post" action="actions/actionSuppressionCategorie.php">
                    <input type="hidden" name="id" value="<?php echo $ligne["id"] ?>">
                    <input type="hidden" name="token" value="<?php echo $token ?>">
                    <button onclick="return confirm('Etes-vous sûr ?')" type="submit" class="btn btn-danger pull-right"><i class="fa fa-trash"></i> Version formulaire direct</button>
                </form>

            </td>

        </tr>
        <?php
    }
    ?>
</table>

</table>
<a href="ajoutZone.php" class="btn btn-success">
    <i class="far fa-plus-circle"></i>
    Ajouter une zone
</a></div>

<?php
include 'footer.php';
mon_footer();
?>
