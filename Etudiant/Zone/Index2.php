<link rel="stylesheet" href="../Style.css">
<?php
include "../HeaderEt.php";
mon_header("Titre etude selec");
?>

<h1>Etudes</h1>

<?php
require "../Config.php";
//creer l'objet PDO qui me connecte a la BDD
$db = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);

$requete = $db->prepare("select Titre, dateDebut, dateFin from etude");
$requete->execute();

$lignes = $requete->fetchAll();

foreach ($lignes

         as $ligne) {
    ?>


    <div id="btn">
        <a href="Index3.php">
            <button type="button" class="list-group-item list-group-item-action active list-group-item-info">
                <?php echo $ligne["Titre"] ?>
            </button>
        </a>
    </div>

    <?php
}
?>
</table>


<a href="ajouterUneEtude.php" class="btn btn-success">
    <i class="far fa-plus-circle"></i>
    Nouvelle étude
</a>

<?php
include '../Footer.php';
mon_footer();
?>