<?php
session_start();
//<div style="height:500px ; width:350px; background-image:url(ifrocean.jpg); background-repeat:initial">
include "../HeaderEt.php";
mon_header("Ajouter une zone");
$token=rand(0,200000000);
$_SESSION["token"]=$token;
?>
<h1>Ajouter une zone </h1>

<form method="post" action="actions/actionAjoutZone.php">
    <input type="hidden" name="token" value="<?php echo $token ?>">
    <div "form-group">
    <h4> Nom de la zone</h4>
    <label for="Nomzone">Nom </label>
    <input type="text" class="form-control" id="Nomzone" maxlength="50"
           name="Nomzone"
           placeholder="Nom de la zone...">
    </div>
    <div "form-group">
    <h4> Mesure marée haute</h4>
    <h5> Point A</h5>
    <label for="lattitude 1 haute mer">lattitude </label>
    <input type="text" class="form-control" id="lattitude 1 haute mer" maxlength="50"
           name="lattitude 1 haute mer"
           placeholder="lattitude 1 haute mer...">
    </div>

    <div "form-group">
    <label for="longitude 1 haute mer">longitude </label>
    <input type="text" class="form-control" id="longitude 1 haute mer" maxlength="50"
           name="longitude 1 haute mer"
           placeholder="longitude 1 haute mer...">
    </div>

    <div "form-group">
    <h5> Point B</h5>

    <label for="lattitude 2 haute mer">lattitude </label>
    <input type="text" class="form-control" id="lattitude 2 haute mer" maxlength="50"
           name="lattitude 2 haute mer"
           placeholder="lattitude 2 haute mer...">
    </div>

    <div "form-group">
    <label for="longitude 2 haute mer">longitude </label>
    <input type="text" class="form-control" id="longitude 2 haute mer" maxlength="50"
           name="longitude 2 haute mer"
           placeholder="longitude 2 haute mer...">
    </div>
    <h4> Mesure marée basse</h4>
    <div "form-group">
    <h5> Point C</h5>
    <label for="lattitude 1 basse mer">lattitude</label>
    <input type="text" class="form-control" id="lattitude 1 basse mer" maxlength="50"
           name="lattitude 1 basse mer"
           placeholder="lattitude 1 basse mer...">
    </div>

    <div "form-group">

    <label for="longitude 1 basse mer">longitude</label>
    <input type="text" class="form-control" id="longitude 1 basse mer" maxlength="50"
           name="longitude 1 basse mer"
           placeholder="longitude 1 basse mer...">
    </div>

    <div "form-group">
    <h5> Point D </h5>
    <label for="lattitude 2 basse mer">lattitude</label>
    <input type="text" class="form-control" id="lattitude 2 basse mer" maxlength="50"
           name="lattitude 2 basse mer"
           placeholder="lattitude 2 basse mer...">
    </div>

    <div "form-group">

    <label for="longitude 2 basse mer">longitude</label>
    <input type="text" class="form-control" id="longitude 2 basse mer" maxlength="50"
           name="longitude 2 basse mer"
           placeholder="longitude 2 basse mer...">

    </div>
    <a href="index.php" class="btn btn-danger pull-left">
        <i class="fal fa-long-arrow-left"></i>
        Retour
    </a>
    <button type="submit" class="btn btn-primary pull-right">Enregistrer</button>
</form></div>
<?php
include '../../Footer.php';
mon_footer();
?>

